#autor: iulian cenusa
#GUI template with tkinter
#--------------------
from tkinter import *
#--------------------
#initializam fereastra
fereastra = Tk()
#--------------------
canvas = Canvas(fereastra,width = 400 , height = 400) #canvas where to draw
canvas.pack()
#by default the lines are black
draw1 = canvas.create_line(0,0,50,50) #starting coordonates / ending coordonates
draw2 = canvas.create_line(50,50,125,150, fill ="red")
draw3 = canvas.create_rectangle(75,75,135,135)
#--------------------
#main loop
fereastra.mainloop()
#--------------------