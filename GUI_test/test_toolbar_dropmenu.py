#autor: iulian cenusa
#GUI template with tkinter
#--------------------
from tkinter import *
#--------------------
def test():
    print ("Test") # functie de test
#--------------------
#initializam fereastra
fereastra = Tk()
#--------------------
menu1 = Menu(fereastra) #meniu
fereastra.config(menu = menu1) #configuram meniul
#--------------------
subMenu1 = Menu(menu1) # submeniu - il punem in primul meniu
menu1.add_cascade(label = "File", menu = subMenu1) #creeaza un buton numit File cu submeniul submenu1
#--------------------
#adaugam functii in submeniu
subMenu1.add_command(label = "NEW" , command = test) # folosi fucntia de mai sus de test
subMenu1.add_command(label = "NEW2" , command = test)
subMenu1.add_separator() #separator in submeniu - pentru estetica
subMenu1.add_command(label = "CLOSE" , command = test)
#--------------------
editM = Menu(menu1)
menu1.add_cascade(label = 'Edit', menu = editM) #meniul de edit
editM.add_command(label = "EDit" , command = test)
#--------------------
#toolbar
toolB = Frame(fereastra,bg = "yellow")
bt1 = Button(toolB , text = "Tool1" , command = test )
bt1.pack(side = LEFT,padx = 2,pady = 2) #padding
toolB.pack(side = TOP,fill = X)
#--------------------
#statusBar
status = Label(fereastra,text = "Status bar" , bd = 1 , relief = SUNKEN, anchor = W)
#status bar with border
status.pack(side=BOTTOM, fill = X)
#--------------------
#main loop
fereastra.mainloop()
#--------------------