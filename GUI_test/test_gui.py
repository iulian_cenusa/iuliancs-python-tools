#autor: iulian cenusa
#GUI testing
#--------------------
from tkinter import *
#--------------------
fereastra = Tk() #fereastra
#parte esentiala
#--------------------
frame1 = Frame(fereastra, width = 600 , height = 400) # frame
#--------------------
frame2 = Frame(fereastra, width = 600 , height = 100) # frame
# we can also have frame.bind()
#--------------------
#test function
def print_name(event):
    print("Test print")
#--------------------
buton1 = Button(frame1, text = "BT1" , fg="blue") #button
#buton1.pack(side = RIGHT)
buton2 = Button(frame1, text = "BT2" , fg="red") #button
#buton2.pack(side = RIGHT)
buton3 = Button(frame1, text = "PRINT" , fg="purple" )#, command = print_name )
# #button with command
#alternatively cand be used event variable
buton3.bind("<Button-1>", print_name)
# event , function
# <Button-1> -left click
# <Button-2> - middle click
# <Button-3> - richt click
#--------------------
label = Label(frame1,text = "Test GUI" , bg="yellow" ) #text
#label.pack(fill=X)
label2 = Label(frame1,text = "Test GUI2" , bg="yellow" ) #text
#label2.pack(fill=X)
#--------------------
#entry - introducere text
en1 = Entry(frame1)
en2 = Entry(frame1)
#--------------------
ck = Checkbutton(frame2, text = "Check Me !!!") #check button - True or False
#--------------------
#layout - grid
label.grid(row = 0 , column = 1 , sticky = W) #sticky = E - east - where to stick
label2.grid(row = 1  , column = 1, sticky = W)
en1.grid(row = 0 , column = 2)
en2.grid(row = 1 , column = 2)
buton1.grid(row = 0 , column = 3)
buton2.grid(row = 1 , column = 3)
buton3.grid(row = 4 , column = 2)
ck.grid(columnspan = 2) # two columns ?
frame1.pack() # put it on window automatically
frame2.pack(side = BOTTOM) # put it on window on bottom
#--------------------
fereastra.mainloop() #main loop of window
#parte esentiala
#--------------------
