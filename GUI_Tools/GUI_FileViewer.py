#autor: iulian cenusa
#27 aug 2019
#GUI file viewer example
#--------------------
from tkinter import *
import os
#--------------------
def test():
    pass
def test_e():
    exit(0)
#--------------------
#initializam fereastra
fereastra = Tk()
#--------------------
#toolbar
toolB = Frame(fereastra)
bt1 = Button(toolB , text = "" , command = test )
bt2 = Button(toolB , text = "Exit" , command = test_e )
bt1.pack(side = LEFT,padx = 2,pady = 2)
bt2.pack(side = LEFT,padx = 2,pady = 2)
toolB.pack(side = TOP,fill = X)
#--------------------
#workzone
frame1 = Frame(fereastra, width = 800 , height = 600) #main zone
frame1.pack()
#--------------------
#title
title = Label(frame1,text = "GUI File Viewer"  )
author = Label(frame1,text = "by Iulian Cenusa" )
title.grid(row = 1 , column = 2 , sticky = N)
author.grid(row = 2 , column = 4 , sticky = N)
#--------------------
#current folder
cur_dir = Label(frame1,text = "Current Directory: " + str(os.getcwd()) )
cur_dir.grid(row = 4 , column = 1 , sticky = W)
#--------------------
#list of files in curent dir
list_dir = Label(frame1,text = "Element List: " + str(os.listdir()))
list_dir.grid(row = 5 , column = 1 , sticky = W)
#--------------------
#show content
show_cnt = Label(frame1,text = "")
def open_file():
    fl = open("text.txt")
    rf = fl.readlines()
    show_cnt.config(text = rf)
#--------------------
#entry and button
en1 = Entry(frame1)
en1.grid(row = 6, column = 1)
btn_open = Button(frame1,text="Open File",fg="red",command = open_file)
btn_open.grid(row = 6, column = 2)
#--------------------
#show content
show_cnt.grid(row = 7, column = 1)
#--------------------
#statusBar
status = Label(fereastra,text = "@IulianCenusa, August 2019" , bd = 1 , relief = SUNKEN, anchor = W)
#status bar with border
status.pack(side=BOTTOM, fill = X)
#--------------------
#main loop
fereastra.mainloop()
#--------------------