#autor: iulian cenusa
#27 aug 2019
#GUI program template with tkinter
#--------------------
from tkinter import *
#--------------------
def test():
    pass
def test_e():
    exit(0)
#--------------------
#initializam fereastra
fereastra = Tk()
#--------------------
#toolbar
toolB = Frame(fereastra)
bt1 = Button(toolB , text = "New" , command = test )
bt2 = Button(toolB , text = "Exit" , command = test_e )
bt1.pack(side = LEFT,padx = 2,pady = 2)
bt2.pack(side = LEFT,padx = 2,pady = 2)
toolB.pack(side = TOP,fill = X)
#--------------------
#workzone
frame1 = Frame(fereastra, width = 800 , height = 600) #main zone
frame1.pack()
#--------------------
#statusBar
status = Label(fereastra,text = "@IulianCenusa, August 2019" , bd = 1 , relief = SUNKEN, anchor = W)
#status bar with border
status.pack(side=BOTTOM, fill = X)
#--------------------
#main loop
fereastra.mainloop()
#--------------------