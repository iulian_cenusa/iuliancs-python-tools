#autor: iulian cenusa
# 21 august 2019
#---------------
import re
#---------------
phone_num = re.compile(r'07\d\d\d\d\d\d\d\d')
#---------------
text = phone_num.search("Am doua numere de telefon: 0726466136 si 0726466135")
print("Numere de telefon gasite: " )
print(text.group()) # one group
print( phone_num.findall("Am doua numere de telefon: 0726466136 si 0726466135") ) # finad all instances
#---------------